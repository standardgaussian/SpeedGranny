window.mt = window.mt || {}; window.mt.data = {
	"assets": {
		"name": "assets",
		"contents": [
			{
				"name": "Loading",
				"contents": [
					{
						"id": 2,
						"name": "loading.png",
						"fullPath": "/Loading/loading.png",
						"key": "/preload.png",
						"width": 190,
						"height": 9,
						"frameWidth": 190,
						"frameHeight": 9,
						"frameMax": -1,
						"margin": 0,
						"spacing": 0,
						"anchorX": 0,
						"anchorY": 0,
						"fps": 10,
						"updated": 1406817917030,
						"atlas": ""
					},
					{
						"id": 1,
						"name": "preload.png",
						"fullPath": "/Loading/preload.png",
						"key": "/loading.png",
						"width": 190,
						"height": 9,
						"frameWidth": 190,
						"frameHeight": 9,
						"frameMax": -1,
						"margin": 0,
						"spacing": 0,
						"anchorX": 0,
						"anchorY": 0,
						"fps": 10,
						"updated": 1406817915033,
						"atlas": ""
					}
				],
				"count": 0,
				"id": 4,
				"fullPath": "/Loading",
				"isClosed": true
			},
			{
				"id": "launcher",
				"name": "launcher.png",
				"fullPath": "/launcher.png",
				"key": "/launcher.png",
				"width": 128,
				"height": 128,
				"frameWidth": 128,
				"frameHeight": 128,
				"frameMax": -1,
				"margin": 0,
				"spacing": 0,
				"anchorX": 0,
				"anchorY": 0,
				"fps": 10,
				"updated": 1406817915034,
				"atlas": ""
			}
		],
		"count": 4
	},
	"objects": {
		"name": "objects",
		"contents": [
			{
				"id": "tmp1406818026618",
				"name": "Loading",
				"x": 0,
				"y": 0,
				"angle": 0,
				"contents": [
					{
						"assetId": 2,
						"x": 256,
						"y": 192,
						"type": 0,
						"anchorX": 0,
						"anchorY": 0,
						"scaleX": 1,
						"scaleY": 1,
						"angle": 0,
						"alpha": 1,
						"frame": 0,
						"isVisible": 1,
						"isLocked": 0,
						"width": 190,
						"height": 9,
						"id": "tmp1406818026617",
						"name": "preload",
						"fullPath": "/Loading/preload",
						"contents": [],
						"assetKey": "/preload.png"
					},
					{
						"assetId": 1,
						"x": 256,
						"y": 192,
						"type": 0,
						"anchorX": 0,
						"anchorY": 0,
						"scaleX": 1,
						"scaleY": 1,
						"angle": 0,
						"alpha": 1,
						"frame": 0,
						"isVisible": 1,
						"isLocked": 0,
						"width": 190,
						"height": 9,
						"id": "tmp1406818026616",
						"name": "loading",
						"fullPath": "/Loading/loading",
						"contents": [],
						"assetKey": "/loading.png"
					}
				],
				"isVisible": false,
				"isLocked": true,
				"isFixedToCamera": 0,
				"fullPath": "/Loading",
				"isClosed": true,
				"movies": {
					"__main": {
						"frames": [],
						"info": {
							"fps": 60
						},
						"subdata": []
					}
				},
				"type": 1
			}
		],
		"count": 0
	},
	"map": {
		"cameraX": -436.42857142857156,
		"cameraY": -129.14285714285714,
		"worldWidth": 400,
		"worldHeight": 480,
		"viewportWidth": 400,
		"viewportHeight": 480,
		"scaleMode": "SHOW_ALL",
		"gridX": 32,
		"gridY": 32,
		"gridOffsetX": 0,
		"gridOffsetY": 0,
		"showGrid": 1,
		"gridOpacity": 0.3,
		"backgroundColor": "#222222",
		"movieInfo": {
			"fps": 60,
			"lastFrame": 60
		},
		"pixelPerfectPicking": 1,
		"physics": {
			"enable": 0
		}
	}
};
