"use strict";
window.SpeedGranny.state.play = {
	preload: function(){
		console.log("loading play state");
	},
	
	create: function(){
		console.log("starting play state");
		
		this.game.physics.startSystem(Phaser.Physics.ARCADE);
		
		//background
		var bg = this.game.make.bitmapData(this.game.world.width, this.game.world.height);
		bg.rect(0,0,this.game.world.width, this.game.world.height, '#000000');
		this.game.add.sprite(0,0,bg);
		
		//make car
		
		var carTexture = this.game.make.bitmapData(20, 90);
		carTexture.rect(0,0, 30, 120, '#FFFFFF');
		
 		this.car = this.game.add.sprite(200, this.game.world.height - 100, carTexture);
		
		//make obstacle prototype
		
		this.barrierTexture = this.game.make.bitmapData(120, 30);
		this.barrierTexture.rect(0,0,120,30, '#FF0000');
		
		//make barrier group
		this.barriers = this.game.add.group();
		
		//make "speed lines"
		
		this.speedLine = this.game.make.bitmapData(10, 70);
		this.speedLine.rect(0,0,20,90, '#00FFFF');
		
		//speed line group
		
		this.speedLines = this.game.add.group();
		
		this.cursors = this.game.input.keyboard.createCursorKeys();
		
		this.game.physics.arcade.enable(this.car);
		this.car.body.collideWorldBounds = true;
		
		//constant vars
		this.trackSpeed = 100;
		this.dodgeSpeed = 150;
		this.lineDensity = 100; //draw a speed line every N updates... really, really crude
		this.obstacleDensity = 0.02;
		
		//tracking variables
		this.lineDraw = 0;
		
		this.boundCallback = this.carCollision.bind(this);
		
		
	},
	
	update: function(){
		//collision
		for (var i = 0; i < this.barriers.length; i++) {
			this.game.physics.arcade.collide(this.car, this.barriers.children[i], this.boundCallback);	
		}
		
		//generate stuff
		//speed lines
		if (++this.lineDraw >= this.lineDensity) {
			this.lineDraw = 0;
			var line = this.game.add.sprite(this.game.world.width/2, 0, this.speedLine);
			this.game.physics.arcade.enable(line);
			this.speedLines.add(line);
		}
		
		//barriers
		if (this.game.rnd.realInRange(0,1) <= this.obstacleDensity) {
			var placement = this.game.rnd.integerInRange(0, this.game.world.width - this.barrierTexture.width);
			var barrier = this.game.add.sprite(placement, 0, this.barrierTexture);
			this.game.physics.arcade.enable(barrier);
			this.barriers.add(barrier);
				
		}
		
		//move stuff
		
		for (var i = 0; i < this.barriers.length; i++) {
			this.barriers.children[i].body.velocity.y = this.trackSpeed;	
		}
		for (var i = 0; i < this.speedLines.length; i++) {
			this.speedLines.children[i].body.velocity.y = this.trackSpeed;	
		}
		
		//input
		
		if (this.cursors.left.isDown) {
			this.car.body.velocity.x = -this.dodgeSpeed;
		}
		else if (this.cursors.right.isDown) {
			this.car.body.velocity.x = this.dodgeSpeed;	
		}
		
		else {
			this.car.body.velocity.x = 0;	
		}
		
		//score kind of
		
		
		
		
	},
	
	carCollision: function(sprite1, sprite2) {
		//We only check if the car collides with a barrier, so just reset the game
		this.game.state.start("load");
	}
};